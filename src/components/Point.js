import React from 'react';
import PropTypes from 'prop-types';

function Point({ coordinates, size, color }) {
  return (
    <mesh position={coordinates}>
      <sphereBufferGeometry attach="geometry" args={[size]} />
      <meshBasicMaterial attach="material" color={color} />
    </mesh>
  );
}

Point.propTypes = {
  coordinates: PropTypes.arrayOf(PropTypes.number).isRequired,
  size: PropTypes.number.isRequired,
  color: PropTypes.string.isRequired
};

export default Point;
