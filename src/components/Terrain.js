import React, { useRef, useMemo } from 'react';
import PropTypes from 'prop-types';
import * as THREE from 'three';

import { generateHeight } from '../utils/geometry';

function Terrain({ onClick }) {
  const group = useRef();

  const [geo, mat] = useMemo(() => {
    const geometry = new THREE.PlaneBufferGeometry(1000, 1000, 255, 255);
    const material = new THREE.MeshBasicMaterial({
      color: new THREE.Color('#272727'),
      side: THREE.DoubleSide,
      opacity: 0.5
    });
    const vertices = geometry.attributes.position.array;
    const heights = generateHeight(256, 256);

    for (let i = 0, j = 0, l = vertices.length; i < l; i++, j += 3) {
      vertices[j + 1] = heights[i] * 3;
    }

    return [geometry, material];
  }, []);

  return (
    <group ref={group}>
      <mesh
        onClick={onClick}
        onPointerOver={e => console.log('hover')}
        onPointerOut={e => console.log('unhover')}
        geometry={geo}
        material={mat}
      />
    </group>
  );
}

Terrain.propTypes = {
  onClick: PropTypes.func.isRequired
};

export default Terrain;
