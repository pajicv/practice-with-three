import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import * as THREE from 'three';
import { useRender } from 'react-three-fiber';
import { Vector3 } from 'three';

function LaserScanner({ position }) {
  const lineRef = useRef();
  let theta = 45;
  useRender(() => {
    // Some things maybe shouldn't be declarative, we're in the render-loop here with full access to the instance
    const startPoint = lineRef.current.geometry.vertices[0];
    const endPoint = new Vector3(
      startPoint.x + 1000 * Math.cos(THREE.Math.degToRad(theta)),
      startPoint.y + 1000 * Math.sin(THREE.Math.degToRad(theta)),
      startPoint.z
    );
    theta += 0.5;
    lineRef.current.geometry = new THREE.Geometry();
    lineRef.current.geometry.vertices.push(startPoint);
    lineRef.current.geometry.vertices.push(endPoint);
  });

  return (
    <group>
      <mesh position={position}>
        <sphereBufferGeometry attach="geometry" args={[10]} />
        <meshBasicMaterial attach="material" color="yellow" />
      </mesh>
      <line ref={lineRef}>
        <geometry
          attach="geometry"
          vertices={[
            new THREE.Vector3(...position),
            new THREE.Vector3(...position)
          ]}
        />
        <lineBasicMaterial attach="material" color="red" />
      </line>
    </group>
  );
}

LaserScanner.propTypes = {
  position: PropTypes.arrayOf(PropTypes.number).isRequired
};

export default LaserScanner;
