import React, { useState } from 'react';
import { Canvas } from 'react-three-fiber';
import Terrain from './components/Terrain';
import Point from './components/Point';
import LaserScanner from './components/LaserScanner';

function App() {
  const [point, setPoint] = useState([0, 0, 0]);

  const handleClick = e => {
    const { x, y, z } = e.point;
    setPoint([x, y, z]);
  };

  return (
    <Canvas camera={{ position: [0, 0, 1000], rotation: [0, 0, 0] }}>
      <ambientLight intensity={0.8} />
      <spotLight intensity={1} position={[0, 0, 0]} angle={0} castShadow />
      <Terrain onClick={handleClick} />
      <Point coordinates={point} />
      <LaserScanner position={[0, 0, 0]} />
    </Canvas>
  );
}

export default App;
